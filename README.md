    
## Quickstart

1. Install dependencies
   ```bash
   pip install -r requrements.txt
   ```
2. Run ES docker
   ```bash
   docker run --rm -p 9200:9200 -p 9300:9300 -e "xpack.security.enabled=false" -e "discovery.type=single-node" -m 1GB docker.elastic.co/elasticsearch/elasticsearch:8.11.1
   ```
3. Run the following script via terminal:
   - Create index
   ```bash
   python create.py
   ```
   - Add data to index
   ```bash
   python insert.py
   ```
   - Perform search in index
   ```bash
   python main.py
   ```
from elasticsearch import Elasticsearch
from base.constants import document_id
import time

start = time.perf_counter()
es = Elasticsearch("http://localhost:9200")

# define index name and user message
index_name = "chapters"
message = "can you tell me about the quarterly results of operations"

"""Sample query when using keywords in search query"""
# Define the query to match documents containing any of the specified keywords
# keywords = ['results of operations', 'quarterly results', 'operations', 'quarterly', 'results']
# es_query = {
#     "query": {
#         "terms": {
#             "Keywords": keywords
#         }
#     }
# }


"""Sample query to return all data from index"""
# Define the query to match all documents
# es_query = {"query": {"match_all": {}}}

"""
Sample boolean query to return all hits where the document_id 
and user message matches any of the fields declared in the query below.

must: All conditions must be met (logical AND).
should: At least one of the conditions should be met (logical OR).
"""
es_query = {
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "document_id": {
                            "query": document_id,
                        }
                    }
                },
                {
                    "nested": {
                        "path": "chunks",
                        "query": {
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "chunks.summary": {
                                                "query": message,
                                                "fuzziness": "AUTO",
                                            }
                                        }
                                    },
                                    {
                                        "match": {
                                            "chunks.headers": {
                                                "query": message,
                                                "fuzziness": "AUTO",
                                            }
                                        }
                                    },
                                    {
                                        "match": {
                                            "chunks.subheaders": {
                                                "query": message,
                                                "fuzziness": "AUTO",
                                            }
                                        }
                                    },
                                    {
                                        "match": {
                                            "chunks.named_entities": {
                                                "query": message,
                                                "fuzziness": "AUTO",
                                            }
                                        }
                                    },
                                ]
                            }
                        },
                        "inner_hits": {},
                    }
                },
            ]
        }
    },
    # "size": 5  # Limit the number of results to 10
}


"""
Sample boolean query using 'more like this query' to return all hits where the document_id 
and user message matches any of the fields declared in the query below.

More Like This Query finds documents that are "like" a given set of documents.
"""
# es_query = {
#     "query": {
#         "bool": {
#             "must": [
#                 {
#                     "match": {
#                         "document_id": {
#                             "query": document_id,
#                         }
#                     }
#                 },
#                 {
#                     "nested": {
#                         "path": "chunks",
#                         "query": {
#                             "more_like_this": {
#                                 "fields": [
#                                     "chunks.summary",
#                                     "chunks.named_entities",
#                                     "chunks.headers",
#                                     "chunks.subheaders",
#                                 ],
#                                 "like": message,
#                                 "min_term_freq": 1,
#                                 "max_query_terms": 12,
#                             }
#                         },
#                         "inner_hits": {},
#                     }
#                 },
#             ]
#         }
#     },
#     "sort": [{"_score": {"order": "desc"}}],
#     "size": 15,  # Limit the number of results to 15
# }


# Simple query to check if the message matches with title field
# es_query = {
#   "query": {
#     "nested": {
#       "path": "chunks",
#       "query": {
#         "match": {"chunks.title" : message}
#       },
#       "inner_hits": {}
#     }
#   }
# }

# Perform the search where body contains the query
result = es.search(index=index_name, body=es_query)
# print(result)

# Extract and print the source of each document
hits = result.get("hits", {}).get("hits", [])

inner_hits = inner_hits = (
    result["hits"]["hits"][0]
    .get("inner_hits", {})
    .get("chunks", {})
    .get("hits", {})
    .get("hits", [])
)

for hit in hits:
    print(hit["_source"])

print("\n------------------------------\n")

# Print inner hits
for hit in inner_hits:
    print("\nInner hit: ")
    print(hit["_source"])

print("\n------------------------------\n")

page_scopes = [item["_source"]["page_scope"] for item in inner_hits]
print("Page scopes: ", page_scopes)

print("\n------------------------------\n")

print("Search done!")
end = time.perf_counter()
print("Time Finished: ", round(end - start, 2))

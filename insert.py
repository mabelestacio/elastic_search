"""
Sample code to add data into the index created
"""


from elasticsearch import Elasticsearch
from base.constants import chapters_list, document_id

# Connect to ES locally
es = Elasticsearch("http://localhost:9200")

# create the format of the items that will be inserted into the index
chapters_body = {"document_id": document_id, "chunks": chapters_list}

# define index name
index_name = "chapters"

# Insert the document id and chapters inside the index
es.index(index=index_name, body=chapters_body)

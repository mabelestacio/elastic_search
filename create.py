"""
Sample code to create an index in ES
"""

from elasticsearch import Elasticsearch
from base.constants import chapters_list, document_id

# Connect to ES locally
es = Elasticsearch("http://localhost:9200")

# Display available index
list_index = es.indices.get_alias(index="*")
print("List of index: ", list_index)

# Define mapping of index to be created
mappings = {
    "properties": {
        "document_id": {"type": "keyword"},
        "chunks": {
            "type": "nested",
            "properties": {
                "summary": {"type": "text", "analyzer": "standard"},
                "headers": {"type": "keyword"},
                "subheaders": {"type": "keyword"},
                "named_entities": {
                    "type": "nested",
                    "dynamic": "true",
                    "properties": {"entity": {"type": "keyword"}},
                },
                "keypoints": {"type": "keyword"},
                "tonality": {"type": "text", "analyzer": "standard"},
                "page_scope": {
                    "type": "object",
                    "properties": {
                        "start_page": {"type": "integer"},
                        "end_page": {"type": "integer"},
                    },
                },
                "total_num_tokens": {"type": "token_count", "analyzer": "standard"},
            },
        },
    }
}

# create index
es.indices.create(index="chapters", mappings=mappings)


"""sample code to create an index if chapters index does not exist with adding data to the index"""
# try:
#     # Check if chapters index already exists
#     chapter_index = es.indices.get_alias(index="chapters")

#     # insert chapter list if "chapters" already exists
#     if chapter_index:
#         print("oh meron na")

#         # create the format of the items that will be inserted into the index
#         chapters_body = {
#             "document_id": document_id,
#             "chunks": chapters_list,
#         }

#         index_name = "chapters"

#         # Insert the document id and chapters inside the index
#         es.index(index=index_name, body=chapters_body)

# except Exception as e:
#     es.indices.create(index="chapters", mappings=mappings)

#     chapters_body = {"document_id": document_id, "chunks": chapters_list}

#     index_name = "chapters"

#     es.index(index=index_name, body=chapters_body)
